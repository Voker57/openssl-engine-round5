#pragma once

int aes_256_gcm_encrypt(unsigned char *plaintext, int plaintext_len, unsigned char *aad,
	int aad_len, unsigned char *key, unsigned char *iv, int iv_len,
	unsigned char *ciphertext, unsigned char *tag, unsigned int tagsize);

int aes_256_gcm_decrypt(unsigned char *ciphertext, int ciphertext_len, unsigned char *aad,
	int aad_len, unsigned char *tag, unsigned int tagsize, unsigned char *key, unsigned char *iv,
	int iv_len, unsigned char *plaintext);
