#pragma once

#include <openssl/evp.h>
#include "round5/params.h"

void round5_register_round5(EVP_PKEY_METHOD *pmeth);

typedef struct {
	unsigned char pk[CRYPTO_PUBLICKEYBYTES];
	unsigned char sk[CRYPTO_SECRETKEYBYTES];
} ROUND5_KEYPAIR;
