#include <string.h>

#include <openssl/rand.h>

#include "round5_meth.h"
#include "round5_objects.h"
#include "round5/api.h"
#include "ossl_aes.h"
#include "debug/debug.h"

#define TAG_SIZE 7
#define IV_SIZE 16

static int round5_keygen(EVP_PKEY_CTX *ctx, EVP_PKEY *pkey) {
	ROUND5_KEYPAIR *kp = OPENSSL_secure_malloc(sizeof(ROUND5_KEYPAIR));
	int nid = NID_round5;
	
	if(kp == NULL)
		return -1;
	
	crypto_kem_keypair(kp->pk, kp->sk);

	EVP_PKEY_assign(pkey, nid, kp);
	
	return 1;
}


static int round5_ctrl(EVP_PKEY_CTX *ctx, int type, int p1, void *p2) {
	int md_nid;
	const char *type_str = "";
	switch(type) {
		default:
			warn("UNSUPPORTED operation (type=%d).\n", type);
			return -2;
	}
	verbose("STUB (type=%s).\n", type_str);
	return 1;
}



static int round5_encrypt(EVP_PKEY_CTX *ctx, unsigned char *out, size_t *outlen, const unsigned char *in,                                              size_t inlen) {
	int ct_size =
		+ CRYPTO_CIPHERTEXTBYTES
		+ TAG_SIZE 
		+ IV_SIZE
		+ inlen;
	if(out == NULL) {
		// This is to probe for size
		*outlen = ct_size;
		return 1;
	}

//	TODO: secure malloc for aeskey?
	char *aeskey = OPENSSL_malloc(32);
	char *tag = out + CRYPTO_CIPHERTEXTBYTES;
	char *iv = out + CRYPTO_CIPHERTEXTBYTES + TAG_SIZE;
	
	EVP_PKEY *pubk = EVP_PKEY_CTX_get0_pkey(ctx);
	if(pubk == NULL) {
		errorf("pubk is NULL\n");
		goto err;
	}
	ROUND5_KEYPAIR *r_keys = EVP_PKEY_get0(pubk);
	if(r_keys == NULL) {
		errorf("r_keys is NULL\n");
		goto err;
	}
	
	if(*outlen < ct_size) {
		errorf("outlen too small: %d/%d\n", *outlen, ct_size);
		goto err;
	}

	int r = RAND_bytes(aeskey, 32);
	if(r < 0) {
		errorf("RAND_bytes failed\n");
		goto err;
	}
	
	r = RAND_bytes(iv, 16);
	if(r < 0) {
		errorf("RAND_bytes failed 2\n");
		goto err;
	}
	
	r = crypto_kem_enc(out, aeskey, r_keys->pk);

	if(r < 0) {
		errorf("key encapsulation failed\n");
		goto err;
	}
	
	r = aes_256_gcm_encrypt(in, inlen, "", 0, aeskey, iv, IV_SIZE, out+CRYPTO_CIPHERTEXTBYTES+TAG_SIZE+IV_SIZE, tag, TAG_SIZE);
	if(r < 0) {
		errorf("AES encryption failed\n");
		goto err;
	}
	
	OPENSSL_free(aeskey);
	return 1;
	
	err:
	OPENSSL_free(aeskey);

	return -1;
}

static int round5_decrypt(EVP_PKEY_CTX *ctx, unsigned char *out, size_t *outlen, const unsigned char *in,                                              size_t inlen) {
	int pt_size = inlen 
		- CRYPTO_CIPHERTEXTBYTES 
		- IV_SIZE 
		- TAG_SIZE;
	if(out == NULL) {
		// This is to probe for size
		*outlen = pt_size;
		return 1;
	}
	
	EVP_PKEY *pubk = EVP_PKEY_CTX_get0_pkey(ctx);
	if(pubk == NULL) {
		errorf("pubk is NULL\n");
		goto err;
	}
	ROUND5_KEYPAIR *r_keys = EVP_PKEY_get0(pubk);
	if(r_keys == NULL) {
		errorf("r_keys is NULL\n");
		goto err;
	}

	char *tag = in + CRYPTO_CIPHERTEXTBYTES;
	char *iv = in + CRYPTO_CIPHERTEXTBYTES + TAG_SIZE;
	
	char *aeskey = OPENSSL_malloc(32);

	if(*outlen < pt_size) {
		errorf("outlen too small: %d/%d\n", *outlen, pt_size);
		goto err;
	}
	
	int r = crypto_kem_dec(aeskey, in, r_keys->sk);
	
	if(r < 0) {
		errorf("key decapsulation failed\n");
		goto err;
	}

	r = aes_256_gcm_decrypt(in+CRYPTO_CIPHERTEXTBYTES+TAG_SIZE+IV_SIZE,
		inlen-(CRYPTO_CIPHERTEXTBYTES+TAG_SIZE+IV_SIZE),
		"", 0, tag, TAG_SIZE, aeskey, iv, IV_SIZE, out);
	
	if(r < 0) {
		goto err;
	}
	
	OPENSSL_free(aeskey);
	return 1;
	
	err:
	OPENSSL_free(aeskey);
	
	return -1;
}

void round5_register_round5(EVP_PKEY_METHOD *pmeth) {
	EVP_PKEY_meth_set_keygen(pmeth, NULL, round5_keygen);
	EVP_PKEY_meth_set_ctrl(pmeth, round5_ctrl, NULL); // TODO: round5_ctrl_str

	EVP_PKEY_meth_set_encrypt(pmeth, NULL, round5_encrypt);
	
	EVP_PKEY_meth_set_decrypt(pmeth, NULL, round5_decrypt);

}
