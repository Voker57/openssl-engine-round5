#include <openssl/err.h>
#include <openssl/x509.h>

#include <string.h>

#include "meths/round5_asn1_meth.h"

#include "round5_keypair.h"
#include "round5_objects.h"
#include "api.h"
#include "debug/debug.h"

#define ROUND5_EVP_PKEY_ASN1_FLAGS 0

typedef enum {
	ROUND5_PUBLIC,
	ROUND5_PRIVATE
} round5_key_op_t;

static int round5_key_print( BIO *bp, const EVP_PKEY *pkey,
							int indent, ASN1_PCTX *ctx, round5_key_op_t op)
{
	if (!pkey)
		return 0;

	const ROUND5_KEYPAIR *kpair = EVP_PKEY_get0(pkey);
	const struct round5_nid_data_st *nid_data = NULL;

	if (op == ROUND5_PRIVATE) {
		if (round5_keypair_is_invalid_private(kpair)) {
			if (BIO_printf(bp, "%*s<INVALID PRIVATE KEY>\n", indent, "") <= 0)
				return 0;
			return 1;
		}
		if (BIO_printf(bp, "%*s%s Private-Key:\n", indent, "", round5_nid_data.name) <= 0)
			return 0;
		if (BIO_printf(bp, "%*spriv:\n", indent, "") <= 0)
			return 0;
		if (ASN1_buf_print(bp, kpair->privk,  round5_nid_data.privk_bytes, indent + 4) == 0)
			return 0;
	} else {
		if (round5_keypair_is_invalid(kpair)) {
			if (BIO_printf(bp, "%*s<INVALID PUBLIC KEY>\n", indent, "") <= 0)
				return 0;
			return 1;
		}
		if (BIO_printf(bp, "%*s%s Public-Key:\n", indent, "", round5_nid_data.name) <= 0)
			return 0;
	}
	if (BIO_printf(bp, "%*spub:\n", indent, "") <= 0)
		return 0;
	if (ASN1_buf_print(bp, kpair->pubk,  round5_nid_data.pubk_bytes,
					indent + 4) == 0)
		return 0;
	return 1;
}

static int round5_priv_print(BIO *bp, const EVP_PKEY *pkey, int indent, ASN1_PCTX *ctx)
{
	return round5_key_print(bp, pkey, indent, ctx, ROUND5_PRIVATE);
}

static int round5_pub_print(BIO *bp, const EVP_PKEY *pkey, int indent, ASN1_PCTX *ctx)
{
	return round5_key_print(bp, pkey, indent, ctx, ROUND5_PUBLIC);
}


static int round5_pub_cmp(const EVP_PKEY *a, const EVP_PKEY *b)
{
	const ROUND5_KEYPAIR *akey = EVP_PKEY_get0(a);
	const ROUND5_KEYPAIR *bkey = EVP_PKEY_get0(b);

	const struct round5_nid_data_st *adata = NULL; // *bdata = NULL;

	if (round5_keypair_is_invalid(akey) || round5_keypair_is_invalid(bkey) )
		return -2;
	return !CRYPTO_memcmp(akey->pubk, bkey->pubk,
						round5_nid_data.pubk_bytes);
}

static void round5_free(EVP_PKEY *pkey)
{
	ROUND5_KEYPAIR *kp = EVP_PKEY_get0(pkey);

	_round5_keypair_free(kp);
}

/* TODO: "parameters" are always equal ? */
static int round5_cmp_parameters(const EVP_PKEY *a, const EVP_PKEY *b)
{
	return 1;
}

static int round5_ctrl(EVP_PKEY *pkey, int op, long arg1, void *arg2)
{
	ROUND5_KEYPAIR *kp = NULL;
	const unsigned char *p = NULL;
	int pklen = 0;

	switch (op) {
	case ASN1_PKEY_CTRL_SET1_TLS_ENCPT:
//         debug("nid: %d, op: ASN1_PKEY_CTRL_SET1_TLS_ENCPT, pklen: %ld\n", nid, arg1);
		p = arg2;
		pklen = arg1;

		if (p == NULL || pklen != round5_nid_data.pubk_bytes ) {
		// SUOLAerr(SUOLA_F_ASN1_GENERIC_CTRL, SUOLA_R_WRONG_LENGTH);
			return 0;
		}

		kp = _round5_keypair_new(NO_PRIV_KEY);
		if (round5_keypair_is_invalid(kp)) {
			return 0;
		}

		memcpy(kp->pubk, p, pklen);

		EVP_PKEY_assign(pkey, NID_round5, kp);
		return 1;


	case ASN1_PKEY_CTRL_GET1_TLS_ENCPT:
//         debug("nid: %d, op: ASN1_PKEY_CTRL_GET1_TLS_ENCPT\n", nid);
		kp = EVP_PKEY_get0(pkey);
		if (!round5_keypair_is_invalid(kp)) {
			unsigned char **ppt = arg2;
			*ppt = OPENSSL_memdup(kp->pubk, round5_nid_data.pubk_bytes);
			if (*ppt != NULL)
				return round5_nid_data.pubk_bytes;
		}
		return 0;
	case ASN1_PKEY_CTRL_DEFAULT_MD_NID:
//         debug("nid: %d, op: ASN1_PKEY_CTRL_DEFAULT_MD_NID, ret: %s\n",
//                 nid, OBJ_nid2sn(nid_data->default_md_nid) );
		*(int *)arg2 = round5_nid_data.default_md_nid;
		return 2;

	default:
		return -2;

	}
}

static int round5_priv_encode(PKCS8_PRIV_KEY_INFO *p8, const EVP_PKEY *pkey)
{
	const ROUND5_KEYPAIR *kp = EVP_PKEY_get0(pkey);
	ASN1_OCTET_STRING oct;
	unsigned char *penc = NULL;
	int penclen;
	char *tmp_buf = NULL;
	int ret = 0;

	if (round5_keypair_is_invalid(kp)) {
//         SUOLAerr(SUOLA_F_ASN1_GENERIC_PRIV_ENCODE, SUOLA_R_INVALID_PRIVATE_KEY);
		return 0;
	}

	tmp_buf = OPENSSL_secure_malloc(round5_nid_data.privk_bytes+round5_nid_data.pubk_bytes);
	if (NULL == tmp_buf) {
//         SUOLAerr(SUOLA_F_ASN1_GENERIC_PRIV_ENCODE, ERR_R_MALLOC_FAILURE);
		return 0;
	}
	

	// TODO: write two octet strings instead?
	memcpy(tmp_buf, kp->privk, round5_nid_data.privk_bytes);
	memcpy(tmp_buf+round5_nid_data.privk_bytes, kp->pubk, round5_nid_data.pubk_bytes);
	oct.data = tmp_buf;
	oct.length = round5_nid_data.privk_bytes+ round5_nid_data.pubk_bytes;
	oct.flags = 0;

// 	 hexx("write keys", oct.data, round5_nid_data.privk_bytes+ round5_nid_data.pubk_bytes);
	
	penclen = i2d_ASN1_OCTET_STRING(&oct, &penc);
	if (penclen < 0) {
//         SUOLAerr(SUOLA_F_ASN1_GENERIC_PRIV_ENCODE, ERR_R_MALLOC_FAILURE);
		ret = 0;
		goto err;
	}

	if (!PKCS8_pkey_set0(p8, OBJ_nid2obj(NID_round5), 0,
						V_ASN1_UNDEF, NULL, penc, penclen)) {
		OPENSSL_clear_free(penc, penclen);
//         SUOLAerr(SUOLA_F_ASN1_GENERIC_PRIV_ENCODE, ERR_R_MALLOC_FAILURE);
		ret = 0;
		goto err;
	}

	ret = 1;
err:
	if (tmp_buf)
		OPENSSL_secure_free(tmp_buf);
	return ret;
}

static int round5_priv_decode(EVP_PKEY *pkey, const PKCS8_PRIV_KEY_INFO *p8)
{
	const unsigned char *p;
	int plen;
	ASN1_OCTET_STRING *oct = NULL;
	const X509_ALGOR *palg;
	ROUND5_KEYPAIR *kp = NULL;

	if (!PKCS8_pkey_get0(NULL, &p, &plen, &palg, p8))
	{
		errorf("get0 failed\n");
		return 0;
	}

	oct = d2i_ASN1_OCTET_STRING(NULL, &p, plen);
	if (oct == NULL) {
		p = NULL;
		plen = 0;
	} else {
		p = ASN1_STRING_get0_data(oct);
		plen = ASN1_STRING_length(oct);
	}

	if (palg != NULL) {
		int ptype;

		/* Algorithm parameters must be absent */
		X509_ALGOR_get0(NULL, &ptype, NULL, palg);
		if (ptype != V_ASN1_UNDEF) {
			errorf("params failed\n");
			return 0;
		}
	}

	if (p == NULL || plen != round5_nid_data.privk_bytes + round5_nid_data.pubk_bytes) {
		errorf("len failed: %d != %d \n", p, plen, round5_nid_data.privk_bytes + round5_nid_data.pubk_bytes);
		return 0;
	}

	kp = _round5_keypair_new(NO_FLAG);
	if (round5_keypair_is_invalid_private(kp)){
		errorf("valid failed\n");
		return 0;
	}
	
	

	memcpy(kp->privk, p, round5_nid_data.privk_bytes);
	memcpy(kp->pubk, p + round5_nid_data.privk_bytes, round5_nid_data.pubk_bytes);
	
	ASN1_OCTET_STRING_free(oct);
	oct = NULL;
	p = NULL;
	plen = 0;

	EVP_PKEY_assign(pkey, NID_round5, kp);

	return 1;
}

static int round5_pub_encode(X509_PUBKEY *pk, const EVP_PKEY *pkey)
{
	const ROUND5_KEYPAIR *kp = EVP_PKEY_get0(pkey);
	unsigned char *penc;

	if (round5_keypair_is_invalid(kp)) {
		errorf("Keypair is invalid\n");
		return 0;
	}

	penc = OPENSSL_memdup(kp->pubk, round5_nid_data.pubk_bytes);
	if (penc == NULL) {
		errorf("memdup failed\n");
		return 0;
	}

	if (!X509_PUBKEY_set0_param(pk, OBJ_nid2obj(NID_round5), V_ASN1_UNDEF,
								NULL, penc, round5_nid_data.pubk_bytes)) {
		OPENSSL_free(penc);
		errorf("malloc failed\n");
		return 0;
	}
	return 1;
}

static int round5_pub_decode(EVP_PKEY *pkey, X509_PUBKEY *pubkey)
{
	const unsigned char *p;
	int pklen;
	X509_ALGOR *palg;
	ROUND5_KEYPAIR *kp = NULL;
	
	if (!X509_PUBKEY_get0_param(NULL, &p, &pklen, &palg, pubkey))
		return 0;

	if (palg != NULL) {
		int ptype;

		/* Algorithm parameters must be absent */
		X509_ALGOR_get0(NULL, &ptype, NULL, palg);
		if (ptype != V_ASN1_UNDEF) {
			errorf("Bogus parameters present\n");
			return 0;
		}
	}

	if (p == NULL || pklen != round5_nid_data.pubk_bytes) {
		errorf("Bad pubk size\n");
		return 0;
	}

	kp = _round5_keypair_new(NO_PRIV_KEY);
	if ( round5_keypair_is_invalid(kp) ){
		errorf("Keypair is invalid");
		return 0;
	}

	memcpy(kp->pubk, p, pklen);

	EVP_PKEY_assign(pkey, NID_round5, kp);
	return 1;
}

static int round5_security_bits(const EVP_PKEY *pkey) {
	// TODO: figure this out
	return 256;
}

static int round5_pubkey_size(const EVP_PKEY *pkey)
{
	return CRYPTO_PUBLICKEYBYTES;
}

static int round5_pubkey_bits(const EVP_PKEY *pkey)
{
	return CRYPTO_PUBLICKEYBYTES * 8;
}

int round5_register_asn1_meth(EVP_PKEY_ASN1_METHOD **ameth, const char *pem_str, const char *info)
{
	debug("REGISTER AMETH NID(%d/%s):%s:%s\n",NID_round5,OBJ_nid2sn(NID_round5),pem_str,info);

	*ameth = EVP_PKEY_asn1_new(NID_round5, ROUND5_EVP_PKEY_ASN1_FLAGS, pem_str, info);
	if (!*ameth)
		return 0;

	EVP_PKEY_asn1_set_public(*ameth, round5_pub_decode, round5_pub_encode, round5_pub_cmp, round5_pub_print, round5_pubkey_size, round5_pubkey_bits);
	EVP_PKEY_asn1_set_private(*ameth, round5_priv_decode, round5_priv_encode, round5_priv_print);
	EVP_PKEY_asn1_set_ctrl(*ameth, round5_ctrl);

	EVP_PKEY_asn1_set_param(*ameth, 0, 0, 0, 0, round5_cmp_parameters, 0);
	EVP_PKEY_asn1_set_security_bits(*ameth, round5_security_bits);
	EVP_PKEY_asn1_set_free(*ameth, round5_free);

	return 1;
}
