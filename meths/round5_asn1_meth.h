#pragma once

#include <openssl/evp.h>

int round5_register_asn1_meth(EVP_PKEY_ASN1_METHOD **ameth, const char *pem_str, const char *info);
