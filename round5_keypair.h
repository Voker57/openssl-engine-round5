#pragma once

#include <openssl/obj_mac.h>
#include "round5/params.h"
#define ROUND5_PUBKEYLEN       CRYPTO_PUBLICKEYBYTES    
#define ROUND5_PRIVKEYLEN      CRYPTO_SECRETKEYBYTES

#include <stdint.h> /* uint8_t */
#include <stddef.h> /* size_t */

typedef struct {
	uint8_t pubk[ROUND5_PUBKEYLEN];
	uint8_t privk[ROUND5_PRIVKEYLEN];
	char has_private;
} ROUND5_KEYPAIR;

typedef enum {
	NO_FLAG=0,
	NO_PRIV_KEY=1,
} round5_keypair_flags_t;

ROUND5_KEYPAIR *_round5_keypair_new(round5_keypair_flags_t flags);

int _round5_keypair_free(ROUND5_KEYPAIR *keypair);

struct round5_nid_data_st {
	const char *name;
	size_t privk_bytes;
	size_t pubk_bytes;
	int default_md_nid;
};

static struct round5_nid_data_st round5_nid_data = { "ROUND5",
		ROUND5_PRIVKEYLEN, ROUND5_PUBKEYLEN, 0};

#define _round5_keypair_is_invalid(kp, contains_private) \
	( (kp) == NULL || ( (contains_private) && (1 != (kp)->has_private) ))

#define round5_keypair_is_invalid(kp) \
	_round5_keypair_is_invalid((kp), 0)
#define round5_keypair_is_invalid_private(kp) \
	_round5_keypair_is_invalid((kp), 1)
