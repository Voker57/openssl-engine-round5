#include <openssl/objects.h>
#include <stdio.h>
#include <stdlib.h>
#include <openssl/engine.h>
#include "meths/round5_meth.h"
#include "meths/round5_asn1_meth.h"
#include "round5_objects.h"
#include "debug/debug.h"

#define sizeof_static_array(a) \
	( (sizeof((a))) / sizeof((a)[0]) )

#define ROUND5_DEBUG_DEFAULT_LEVEL LOG_WARN
#define ROUND5_DEBUG_ENVVAR "ROUND5_DEBUG"
	
static const char *engine_id = "openssl-engine-round5";
static const char *engine_name = "Round5 OpenSSL engine.";

static EVP_PKEY_METHOD *pmeth_round5 = NULL;
static EVP_PKEY_ASN1_METHOD *ameth_round5 = NULL;

static int round5_pkey_meth_nids[] = {
	0 /* NID_round5 */,
	0
};

static int round5_ameth_nids[] = {
	0 /* NID_round5 */,
	0
};

static void round5_pkey_meth_nids_init()
{
	round5_pkey_meth_nids[0] = NID_round5;
}

static void round5_ameth_nids_init()
{
	round5_ameth_nids[0] = NID_round5;
}

static int round5_e_init(ENGINE *e)
{
	return 1;
}

static int round5_e_destroy(ENGINE *e)
{
	debug_logging_finish();
	OBJ_cleanup();
	return 1;
}

static int round5_e_finish(ENGINE *e)
{
	return 1;
}


int register_nids() {
	char *oid_str = "1.2.4.5.6.7";
	char *sn = "ROUND5";
	char *ln = "ROUND5";
	int new_nid = NID_undef;

	if (NID_undef != (new_nid = OBJ_sn2nid(sn)) ) {
		debug("'%s' is already registered with NID %d\n", sn, new_nid);
		return new_nid;
	}

	new_nid = OBJ_create(oid_str, sn, ln);

	if (new_nid == NID_undef) {
		fatalf("Failed to register NID for '%s'\n", ln);
		return 0;
	}
	debug("Registered '%s' with NID %d\n", sn, new_nid);

	ASN1_OBJECT *obj = OBJ_nid2obj(new_nid);
	if ( !obj ) {
		errorf("objfaile\n");
		fatalf("Failed to retrieve ASN1_OBJECT for dinamically registered NID\n");
		return 0;
	}

	if ( NID_undef == new_nid ) {
		errorf("Failed to register NID for '%s'\n", sn );
		return 0;
	}
	
	
	NID_round5 = new_nid;
	
	return 1;
}

static int round5_register_pmeth(EVP_PKEY_METHOD **pmeth, int flags)
{
	*pmeth = EVP_PKEY_meth_new(NID_round5, flags);

	if (*pmeth == NULL)
		return 0;
	round5_register_round5(*pmeth);

	return 1;
}

static int round5_register_ameth(EVP_PKEY_ASN1_METHOD **ameth, int flags)
{
	const char *pem_str = NULL;
	const char *info = NULL;

	if (!ameth)
		return 0;


		if (!OBJ_add_sigid(NID_dsa, NID_sha512, NID_round5)) {
			errorf("OBJ_add_sigid() failed\n");
			return 0;
		}

	pem_str = OBJ_nid2sn(NID_round5);
	
	info = OBJ_nid2ln(NID_round5);
	return round5_register_asn1_meth(ameth, pem_str, info);
}

static int round5_register_methods()
{
	return round5_register_pmeth(&pmeth_round5, 0) &&
	round5_register_ameth(&ameth_round5, 0);
}

static int round5_pkey_meths(ENGINE *e, EVP_PKEY_METHOD **pmeth, const int **nids, int nid)
{

	if(!pmeth) {
		*nids = round5_pkey_meth_nids;
		return sizeof_static_array(round5_pkey_meth_nids) - 1;
	}
	debug("NID(%d/%s) ->\n", nid, OBJ_nid2sn(nid));
	if (nid == NID_round5) {
		*pmeth = pmeth_round5;
		return 1;
	}
	
	debug("requested nid not found\n");
	*pmeth = NULL;
	return 0;
}

static int round5_ameths(ENGINE *e, EVP_PKEY_ASN1_METHOD **ameth, const int **nids, int nid)
{
	if(!ameth) {
		*nids = round5_ameth_nids;
		return sizeof_static_array(round5_ameth_nids) - 1;
	}
	debug("NID(%d/%s) ->", nid, OBJ_nid2sn(nid));

	if (nid == NID_round5) {
		*ameth = ameth_round5;
		return 1;
	}
	
	errorf("requested nid not found\n");
	*ameth = NULL;
	return 0;
}



static int round5_bind(ENGINE *e, const char *id)
{
	debug_logging_init(ROUND5_DEBUG_DEFAULT_LEVEL, ROUND5_DEBUG_ENVVAR);
	
	int ret = 0;
	if (!ENGINE_set_id(e, engine_id)) {
		errorf("ENGINE_set_id failed\n");
		goto end;
	}
	if (!ENGINE_set_name(e, engine_name)) {
		errorf("ENGINE_set_name failed\n");
		goto end;
	}

	if(!ENGINE_set_init_function(e, round5_e_init)) {
		errorf("ENGINE_set_init_function failed\n");
		goto end;
	}
	if(!ENGINE_set_destroy_function(e, round5_e_destroy)) {
		errorf("ENGINE_set_destroy_function failed\n");
		goto end;
	}
	if(!ENGINE_set_finish_function(e, round5_e_finish)) {
		errorf("ENGINE_set_finish_function failed\n");
		goto end;
	}

	if (!register_nids()) {
		errorf("Failure registering NIDs\n");
		goto end;
	}
	
	round5_pkey_meth_nids_init();
	round5_ameth_nids_init();

	if (!round5_register_methods()) {
		errorf("Failure registering methods\n");
		goto end;
	}

	if (!ENGINE_set_pkey_meths(e, round5_pkey_meths)) {
		errorf("ENGINE_set_pkey_meths failed\n");
		goto end;
	}
	
	if (!ENGINE_set_pkey_asn1_meths(e, round5_ameths)) {
		errorf("ENGINE_set_pkey_meths failed\n");
		goto end;
	}

	ret = 1;
end:
	return ret;
}


IMPLEMENT_DYNAMIC_BIND_FN(round5_bind)
IMPLEMENT_DYNAMIC_CHECK_FN()
