#include "round5_keypair.h"
#include "round5_objects.h"

#include <openssl/crypto.h>
#include <openssl/ec.h>
#include <openssl/err.h>

ROUND5_KEYPAIR *_round5_keypair_new(round5_keypair_flags_t flags) {
	ROUND5_KEYPAIR *kpair = NULL;

	kpair = OPENSSL_secure_malloc(sizeof(*kpair));
	if (kpair == NULL) {
		goto err;
	}

	kpair->has_private = 0;

	if (0 == (flags & NO_PRIV_KEY) ){
		kpair->has_private = 1;
	}

	return kpair;
err:
	if (kpair)
		OPENSSL_secure_free(kpair);

	return NULL;
}

int _round5_keypair_free(ROUND5_KEYPAIR *keypair) {
	if (!keypair)
		return 0;

	OPENSSL_secure_free(keypair);

	return 1;
}
